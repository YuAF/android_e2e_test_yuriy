package com.mysuperdispatch.android.end2end.action;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

/**
 * Created by bahrom on 5/3/17.
 */

public class LoginActions {

    public static void login(String emailAddress, String passWord, Boolean useSuperLink, int delayBetweenInterfaces, UiDevice currDevice) throws UiObjectNotFoundException, InterruptedException {
        //Find and click the button with 'GET STARTED' text.
        UiObject2 GetStartedButton = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.Button")
                .text("Get Started"), currDevice);
        GetStartedButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

        //Find and fill email field. Entered email should exist in the system as email of an active account.
        UiObject2 EmailField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/emailInput"), currDevice);
        EmailField.setText(emailAddress);

        // Find and click on Next button ...
        UiObject2 NextButton = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.Button")
                .text("Next"), currDevice);
        NextButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);


        // if login with password intended to be tested, perform appropriate sequence
        if (!useSuperLink) {

            // Find and tap Type Password button
            UiObject2 typePasswordButton = CommonActions.getTheObjectOrFail(By
                    .text("Type Password")
                    .clazz("android.widget.Button"), currDevice);
            typePasswordButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

            // Find the Enter password hint
            UiObject2 enterPasswordHint = CommonActions.getTheObjectOrFail(By
                    .text("Enter Password"), currDevice);

            // Find and fill password field.
            UiObject2 passwordFieldWithinLoginForm = CommonActions.getTheObjectOrFail(By
                    .clazz("android.widget.EditText")
                    .res("com.mysuperdispatch.android:id/passwordInput"), currDevice);
            passwordFieldWithinLoginForm.setText(passWord);

            // Find and tap Log In button.
            UiObject2 loginButton = CommonActions.getTheObjectOrFail(By
                    .text("Log In")
                    .clazz("android.widget.Button"), currDevice);
            loginButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces * 2);

        }

        // if login using Super Link intended to be tested, perform appropriate sequence
        else {

            // Tap Send SuperLink button
            UiObject2 sendSuperLinkButton = CommonActions.getTheObjectOrFail(By
                    .text("Type Password")
                    .clazz("android.widget.Button"), currDevice);
            sendSuperLinkButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

            // Check that message is displayed
            UiObject2 superLinkIsSentMessage = CommonActions.getTheObjectOrFail(By
                    .clazz("android.widget.TextView")
                    .text("Super link has been sent to your email"), currDevice);

            currDevice.pressHome();

            // Open Chrome browser
            CommonActions.launchApplication("com.android.chrome");

            UiObject2 addressBar = CommonActions.getTheObjectOrFail(By
                    .res("com.android.chrome:id/url_bar"), currDevice);
            addressBar.clear();
            addressBar.setText("gmail.com");
            currDevice.pressEnter();
            Thread.sleep(delayBetweenInterfaces*3);


        }

        // Find and fill email field within login form.
        // (YuF 07.04.2017): This functionality should be reviewed, Trello card is created - https://trello.com/c/NgPtP00T/26-interface-android-ios-email-is-available-for-edit-after-verification-within-get-started-operation.
        // (YuF 12.05.2017): commented according changes
        /*
        UiObject2 EmailFieldWithinLoginForm = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/emailT"), currDevice);
        EmailFieldWithinLoginForm.setText(emailAddress);
        */


        // Close all notifications, if appeared
        //CommonActions.closeNotifications(currDevice, delayBetweenInterfaces);

        // Close intercom notifications, if appeared
        CommonActions.closeIntercomNotifications(currDevice, delayBetweenInterfaces);

        // After handling known notifucations, verify that user is logged in.
        CommonActions.verifyThatLoggedIn(currDevice);
    }

    public static void logout(UiDevice currDevice) throws UiObjectNotFoundException, InterruptedException {
        // Find Profile icon, click on it.
        UiObject2 profileIcon = CommonActions.getTheObjectOrFail(By
                .res("com.mysuperdispatch.android:id/action_profile")
                .clazz("android.widget.TextView")
                .desc("Profile"), currDevice);
        profileIcon.clickAndWait(Until.newWindow(), 1500);

        // Scroll settings page down to find Logout link.
        UiScrollable scrollableProfileSettings = new UiScrollable(new UiSelector()
                .scrollable(true)
                .className("android.widget.ScrollView"));
        scrollableProfileSettings.scrollToEnd(scrollableProfileSettings.getMaxSearchSwipes());

        // Find Logout link and click on it.
        UiObject2 logoutLink = CommonActions.getTheObjectOrFail(By
                .text("Log Out")
                .clazz("android.widget.TextView"), currDevice);
        logoutLink.clickAndWait(Until.newWindow(), 1500);

        //Verify that Into interface is opened
        UiObject2 GetStartedButtonAfterLogout = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.Button")
                .text("Get Started"), currDevice);
    }
}
