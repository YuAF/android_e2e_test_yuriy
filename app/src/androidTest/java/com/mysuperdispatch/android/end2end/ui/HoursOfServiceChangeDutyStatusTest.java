package com.mysuperdispatch.android.end2end.ui;

import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObjectNotFoundException;

import com.mysuperdispatch.android.end2end.action.CommonActions;
import com.mysuperdispatch.android.end2end.action.HoursOfServiceActions;
import com.mysuperdispatch.android.end2end.action.MainActions;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import static com.mysuperdispatch.android.end2end.action.LoginActions.login;
import static com.mysuperdispatch.android.end2end.action.LoginActions.logout;

/**
 * Created by yuriy on 03.08.17.
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 21)
public class HoursOfServiceChangeDutyStatusTest extends BaseTest {

    private static UiDevice theDevice;
    private static String theLocationValue;
    private static String theNotesValue;
    private static String errorDutyStatusIsNotActive = "Defined Duty Status is not active!";


    @BeforeClass
    public static void setUpOnce() throws InterruptedException, UiObjectNotFoundException {
        theDevice = CommonActions.getCurrentUIDevice();
        assertNotNull(theDevice);
        theDevice.pressHome();
        CommonActions.launchApplication("com.mysuperdispatch.android");

        // Login using password authentification
        //login("yuriy@mysuperdispatch.com", "yuriy", false, 1500, theDevice);

        // Open Hours Of Service interface
        MainActions.openHoSTab(theDevice);
    }

    @Before
    public void setupTheEnvironment() throws InterruptedException {
        theLocationValue = "This is location " + System.currentTimeMillis() + "";
        theNotesValue = "These are notes." + System.currentTimeMillis() + "";

        // if Main interface is not opened, perfrom Back navigation
        while (!MainActions.isMainInterfaceDisplayed(theDevice)) {
            MainActions.navigateBack(theDevice);
        }
    }

    @Test
    public void testChangeDutyStatus() throws InterruptedException {

        HoursOfServiceActions.changeDutyStatus("On Duty", "Driving", "This is location", "These are notes.", theDevice);
        assertTrue(errorDutyStatusIsNotActive, HoursOfServiceActions.isTheDutyStatusButtonSelected("Driving", theDevice));
    }

    @Test
    public void testLocationAndNotesValueSaving() throws InterruptedException {
        HoursOfServiceActions.changeDutyStatus("On Duty", "Driving", theLocationValue, theNotesValue, theDevice);
        assertTrue(HoursOfServiceActions.ChangeDutyStatusActions.isLocationValueEquals(theLocationValue, theDevice));
        assertTrue(HoursOfServiceActions.ChangeDutyStatusActions.isNotesValueEquals(theNotesValue, theDevice));
    }

    //@Test
    public void testDrivingTimeLeftValue() throws InterruptedException, UiObjectNotFoundException {

        HoursOfServiceActions.changeDutyStatus("On Duty", "Driving", theLocationValue, theNotesValue, theDevice);
        HoursOfServiceActions.scrollHoSInterface(999, theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(2, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, "AM", 12, 0, "AM", "OFF", theDevice);
        MainActions.navigateBack(theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(3, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, "AM", 12, 0, "AM", "OFF", theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(1, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, "AM", 12, 0, "AM", "OFF", theDevice);
        HoursOfServiceActions.DailyLogActions.openDutyStatusActivityRecord(2, theDevice);
        HoursOfServiceActions.DailyLogActions.DutyStatusActivityRecordActions.selectDutyStatusButton("OFF", theDevice);
        HoursOfServiceActions.DailyLogActions.DutyStatusActivityRecordActions.saveChanges(theDevice);
        MainActions.navigateBack(theDevice);
        HoursOfServiceActions.scrollHoSInterface(-999, theDevice);
        HoursOfServiceActions.changeDutyStatus("On Duty", "Driving", theLocationValue, theNotesValue, theDevice);

    }

    public void testSeventyHourDutyLimitChart() throws InterruptedException, UiObjectNotFoundException {

    }

}
