package com.mysuperdispatch.android.end2end.action;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

/**
 * Created by yuriy on 26.07.17.
 */

public class MainActions {

    // Open Hours Of Servcie tab
    public static void openHoSTab(UiDevice currDevice) throws UiObjectNotFoundException, InterruptedException {

        CommonActions.getTheObjectOrFail(By.res("com.mysuperdispatch.android:id/action_item_hos"), currDevice).click();
    }

    public static void navigateBack(UiDevice currDevice) throws InterruptedException {
        CommonActions.getTheObjectOrFail(By.clazz("android.widget.ImageButton").desc("Navigate up"), currDevice).click();
    }

    public static Boolean isMainInterfaceDisplayed(UiDevice currdevice) throws InterruptedException {
        if (CommonActions.getTheObjectOrNull(By
                .res("com.mysuperdispatch.android:id/action_bar")
                .clazz("android.view.ViewGroup")
                .hasChild(By.text("Super Dispatch")), currdevice) != null) {
            return true;
        }
        else {
            return false;
        }
    }

}
