package com.mysuperdispatch.android.end2end.action;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.BySelector;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.Canvas;
import android.view.View;
import android.graphics.Color;

import static org.junit.Assert.fail;

/**
 * Created by yuriy on 06.04.17.
 */

public class CommonActions {

    private static final String locationInputResourceID = "com.mysuperdispatch.android:id/locationInput";

    public static UiDevice getCurrentUIDevice() {
        UiDevice currDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        return currDevice;
    }

    // Launch an application
    public static void launchApplication(String PackageName) {
        Context context = InstrumentationRegistry.getInstrumentation().getContext();
        Intent intent = context.getPackageManager().getLaunchIntentForPackage(PackageName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }


    // Object identification
    public static UiObject2 getTheObjectOrFail(BySelector selector, UiDevice crDevice) throws InterruptedException {
        UiObject2 object = null;
        int timeout = 10000;
        int delay = 100;
        long time = System.currentTimeMillis();
        Thread.sleep(500); //delay before proceed
        while (object == null) {
            object = crDevice.findObject(selector);
            Thread.sleep(delay);
            if (System.currentTimeMillis() - timeout > time) {
                fail();
            }
        }
        return object;
    }


    //Tap button with defined text
    public static void tapTheButton(String buttonText, UiDevice currentDevice) throws InterruptedException {
        getTheObjectOrFail(By
                .clazz("android.widget.Button")
                .text(buttonText), currentDevice).click();
    }

    public static void tapTheRadioButton(String radioButtonText, UiDevice currentDevice) throws InterruptedException {
        getTheObjectOrFail(By
                .clazz("android.widget.RadioButton")
                .text(radioButtonText), currentDevice).click();
    }

    public static void tapItemUnderTheLabel(String labelText, UiDevice currentDevice) throws InterruptedException {
        CommonActions.getTheObjectOrFail(By.text(labelText), currentDevice).getParent().getChildren().get(1).click();
    }

    public static void selectClockSign(int signValue, UiDevice currentDevice) throws InterruptedException {
        getTheObjectOrFail(By
                .clazz("android.widget.RadialTimePickerView$RadialPickerTouchHelper")
                .desc(Integer.toString(signValue)), currentDevice).click();
    }



    public static UiObject2 getByResourceID(String resourceID, UiDevice currentDevice) throws InterruptedException {
        return getTheObjectOrFail(By.res(resourceID), currentDevice);
    }

    public static void fillByResourceID(String resourceID, String textValue, UiDevice currentDevice) throws InterruptedException {
        getByResourceID(resourceID, currentDevice).setText(textValue);
    }

    public static void performSetCurrentLocation(UiDevice currentDevice) throws InterruptedException {
        getTheObjectOrFail(By.res(locationInputResourceID), currentDevice).getParent().getParent().getParent().getChildren().get(1).click();
        Thread.sleep(10000);
    }


    // Object identification with null instead of error on not found
    public static UiObject2 getTheObjectOrNull(BySelector selector, UiDevice currDevice) throws InterruptedException {
        UiObject2 object = null;
        int timeout = 5000;
        int delay = 100;
        long time = System.currentTimeMillis();
        Thread.sleep(500); //delay before proceed
        while (object == null) {
            object = currDevice.findObject(selector);
            Thread.sleep(delay);
            if (System.currentTimeMillis() - timeout > time) {
                return null;
            }
        }
        return object;
    }

    /*//By resource ID value
    public static UiObject2 getTheObjectOrFailByResourceID(String resourceID, UiDevice currDevice) throws InterruptedException {
        UiObject2 object = null;
        int timeout = 10000;
        int delay = 1000;
        long time = System.currentTimeMillis();
        while (object == null) {
            object = currDevice.findObject(By.res(resourceID));
            Thread.sleep(delay);
            if (System.currentTimeMillis() - timeout > time) {
                fail();
            }
        }
        return object;
    }

    // By text ends with
    public static UiObject2 getTheObjectOrFailByTextEndsWith(String endOfTheText, UiDevice currDevice) throws InterruptedException {
        UiObject2 object = null;
        int timeout = 10000;
        int delay = 1000;
        long time = System.currentTimeMillis();
        while (object == null) {
            object = currDevice.findObject(By.textEndsWith(endOfTheText));
            Thread.sleep(delay);
            if (System.currentTimeMillis() - timeout > time) {
                fail();
            }
        }
        return object;
    }

    // get by child
    public static UiObject2 getTheObjectOrFailByChild(String endOfTheText, UiDevice currDevice) throws InterruptedException {
        UiObject2 object = null;
        int timeout = 10000;
        int delay = 1000;
        long time = System.currentTimeMillis();
        while (object == null) {
            object = currDevice.findObject(By.hasChild(By.textEndsWith(endOfTheText)));
            Thread.sleep(delay);
            if (System.currentTimeMillis() - timeout > time) {
                fail();
            }
        }
        return object;
    }*/

    // Scroll defined interface
    public static void scrollTheInterface(int stepsCount, UiDevice currDevice) throws InterruptedException, UiObjectNotFoundException {

        UiScrollable scrollableInterface = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView")
                .scrollable(true));

        //if 999 is specified as steps count, scroll to the end of the interface
        if (stepsCount == 999) {
            scrollableInterface.scrollToEnd(scrollableInterface.getMaxSearchSwipes());
        }
        else {
            if (stepsCount == -999)
            {
                scrollableInterface.scrollToBeginning(scrollableInterface.getMaxSearchSwipes());
            }
            else
            {
                if (stepsCount > 0) {
                    for (int i=1;i<=stepsCount;i++) {
                        scrollableInterface.scrollForward();
                    }
                }
                else {
                    for (int i=-1;i>=stepsCount;i--) {
                        scrollableInterface.scrollBackward();
                    }
                }
            }
        }

    }

    // Close intercom notifications, if exist
    public static void closeIntercomNotifications(UiDevice theDevice, int delayBetweenInterfaces) throws InterruptedException {

        // Check if any intercom notification is appeared, click on close button if appeared
        while (CommonActions.getTheObjectOrNull(By
                .res("com.mysuperdispatch.android:id/intercom_toolbar"), theDevice) != null) {
            UiObject2 closeButtonWithinNotification = CommonActions.getTheObjectOrFail(By
                    .clazz("android.widget.ImageButton")
                    .res("com.mysuperdispatch.android:id/intercom_toolbar_close"), theDevice);
            closeButtonWithinNotification.click();
        }
    }

    // Close modal notifications. Need to be redeveloped.
    public static void closeNotifications(UiDevice theDevice, int delayBetweenInterfaces) throws InterruptedException {
        // Check if Update App message form is appeared, click on Dismiss button if appeared.
        UiObject2 updateAppMessage = CommonActions.getTheObjectOrNull(By
                .text("Update Your App")
                .clazz("android.widget.TextView"), theDevice);
        if (updateAppMessage != null) {
            UiObject2 dismissButtonInUpdateAppMessage = CommonActions.getTheObjectOrFail(By
                    .text("Dismiss")
                    .clazz("android.widget.Button"), theDevice);
            dismissButtonInUpdateAppMessage.click();
        }

        //Check if Google Play services message form is appeared, click on OK button if appeared.
        UiObject2 googlePlayServicesMessage = CommonActions.getTheObjectOrNull(By
                .text("Super Dispatch relies on Google Play services, which is not supported by your device. Contact the manufacturer for assistance.")
                .clazz("android.widget.TextView"), theDevice);
        if (googlePlayServicesMessage != null) {
            UiObject2 okButtonInGooglePlayServicesMessageForm = CommonActions.getTheObjectOrFail(By
                    .text("OK")
                    .clazz("android.widget.Button"), theDevice);
            okButtonInGooglePlayServicesMessageForm.click();
        }

        // Check if any notification is appeared, click on close button if appeared
        while (CommonActions.getTheObjectOrNull(By
                .res("com.mysuperdispatch.android:id/note_layout")
                .clazz("android.widget.LinearLayout"), theDevice) != null) {
            UiObject2 closeButtonWithinNotification = CommonActions.getTheObjectOrFail(By
                    .clazz("android.widget.ImageButton")
                    .res("com.mysuperdispatch.android:id/intercom_toolbar_close"), theDevice);
            closeButtonWithinNotification.click();
        }
    }

    // Verify that user is logged in
    public static void verifyThatLoggedIn(UiDevice theDevice) throws InterruptedException {
        // Verify Super Dispatch label.
        UiObject2 superDispatchLabel = CommonActions.getTheObjectOrFail(By
                .text("Super Dispatch")
                .clazz("android.widget.TextView"), theDevice);
        //assertNotNull(superDispatchLabel);

        // Verify Profile icon.
        UiObject2 profileIcon = CommonActions.getTheObjectOrFail(By
                .res("com.mysuperdispatch.android:id/action_profile")
                .clazz("android.widget.TextView")
                .desc("Profile"), theDevice);
        //assertNotNull(profileIcon);
    }


    // Exit the application
    public static void exitApplicationSequentally(UiDevice currDevice) {
        //Define the app icon
        UiObject2 AppItemInMenu = currDevice.findObject(By
                .text("Super Dispatch")
                .clazz("android.widget.TextView"));

        // Press back button while icon doesn't exist
        while (AppItemInMenu.toString() == null) {
            currDevice.pressBack();
            try {
                Thread.sleep(1000);                 //1000 milliseconds is one second.
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }

    }

    /*
    public static Bitmap getScreenPartImage() {

        Bitmap bitmap;
        ViewGroup v1 = findViewById(R.id.layout_id);
        v1.setDrawingCacheEnabled(true);
        bitmap = Bitmap.createBitmap(v1.getDrawingCache());
        v1.setDrawingCacheEnabled(false);

    }
    */

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

}
