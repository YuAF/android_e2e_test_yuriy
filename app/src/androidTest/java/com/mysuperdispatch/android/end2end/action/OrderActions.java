package com.mysuperdispatch.android.end2end.action;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

/**
 * Created by yuriy on 14.04.17.
 */

public class OrderActions {

    // Create Order
    public static void createOrder(int delayBetweenInterfaces, UiDevice currDevice) throws InterruptedException {
        // Find and click on New link
        UiObject2 newOrderLink = CommonActions.getTheObjectOrFail(By
                .text("New")
                .res("com.mysuperdispatch.android:id/action_new_inspection"), currDevice);
        newOrderLink.clickAndWait(Until.newWindow(), delayBetweenInterfaces);
        //
    }

    // Enter the pickup info data:
    public static void enterPickupInformationOfOrder(String loadID, String nameOfPickupInfo, String contactNameOfPickupInfo, String addressOfPickupInfo, String cityOfPickupInfo, String stateOfPickupInfo, String zipCodeOfPickupInfo, String phoneNumberOfPickupInfo, String notesOfPickupInfo, Boolean addUniqueSuffixToValues, Integer delayBetweenInterfaces, UiDevice currDevice) throws InterruptedException, UiObjectNotFoundException {
        if (addUniqueSuffixToValues) {
            String uniqueSuffix = System.currentTimeMillis() + "";
            loadID = uniqueSuffix + "_" + loadID;
            nameOfPickupInfo = nameOfPickupInfo + "_" + uniqueSuffix;
            contactNameOfPickupInfo = contactNameOfPickupInfo + "_" + uniqueSuffix;
            addressOfPickupInfo = addressOfPickupInfo + "_" + uniqueSuffix;
            //cityOfPickupInfo = cityOfPickupInfo + "_" + uniqueSuffix;
            //stateOfPickupInfo = stateOfPickupInfo + "_" + uniqueSuffix;
            //zipCodeOfPickupInfo = zipCodeOfPickupInfo + "_" + uniqueSuffix;
            notesOfPickupInfo = notesOfPickupInfo + "_" + uniqueSuffix;
        }

        // Find and click on Add Pickup Information button
        UiObject2 addPickupInfoButton = CommonActions.getTheObjectOrFail(By
                .text("Add Pickup Information")
                .clazz("android.widget.Button"), currDevice);
        addPickupInfoButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

        // Find and fill Load ID field
        UiObject2 loadIDField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/loadIdInput"), currDevice);
        loadIDField.setText(loadID);

        // Find and fill Name field
        UiObject2 nameField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/nameInput"), currDevice);
        nameField.setText(nameOfPickupInfo);

        // Find and fill contact name field
        UiObject2 contactNameField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/contactInput"), currDevice);
        contactNameField.setText(contactNameOfPickupInfo);

        // Find and fill Address field
        UiObject2 addressOfPickupInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/addressInput"), currDevice);
        addressOfPickupInfoField.setText(addressOfPickupInfo);

        // Scroll down to get access to other fields
        UiScrollable pickupInfoForm = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView")
                .scrollable(true));
        pickupInfoForm.scrollForward();

        // Find and fill city field
        UiObject2 cityOfPickupInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/cityInput"), currDevice);
        cityOfPickupInfoField.setText(cityOfPickupInfo);

        // Find and fill state field
        UiObject2 stateOfPickupInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/stateInput"), currDevice);
        stateOfPickupInfoField.setText(stateOfPickupInfo);

        // Find and fill zip code field
        UiObject2 zipCodeOfPickupInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/zipInput"), currDevice);
        zipCodeOfPickupInfoField.setText(zipCodeOfPickupInfo);

        // Scroll down to get access to other fields
        pickupInfoForm.scrollForward();

        // Find and fill phone number field
        UiObject2 phoneNumberOfPickupInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/phoneInput"), currDevice);
        phoneNumberOfPickupInfoField.setText(phoneNumberOfPickupInfo);

        // Find and fill notes field
        UiObject2 notesOfPickupInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/notesInput"), currDevice);
        notesOfPickupInfoField.setText(notesOfPickupInfo);

        // Find and click on Save link
        UiObject2 saveButton = CommonActions.getTheObjectOrFail(By
                .text("Save")
                .res("com.mysuperdispatch.android:id/action_save")
                .clazz("android.widget.TextView"), currDevice);
        saveButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);
    }
    // End of enterPickupInformationOfOrder


    // Enter the delivery info data
    public static void enterDeliveryInfoOfOrder(String nameOfDeliveryInfo, String contactNameOfDeliveryInfo, String addressOfDeliveryInfo, String cityOfDeliveryInfo, String stateOfDeliveryInfo, String zipCodeOfDeliveryInfo, String phoneNumberOfDeliveryInfo, String notesOfDeliveryInfo, Boolean addUniqueSuffixToValues, Integer delayBetweenInterfaces, UiDevice currDevice) throws InterruptedException, UiObjectNotFoundException {
        if (addUniqueSuffixToValues) {
            String uniqueSuffix = System.currentTimeMillis() + "";
            nameOfDeliveryInfo = nameOfDeliveryInfo + "_" + uniqueSuffix;
            contactNameOfDeliveryInfo = contactNameOfDeliveryInfo + "_" + uniqueSuffix;
            addressOfDeliveryInfo = addressOfDeliveryInfo + "_" + uniqueSuffix;
            notesOfDeliveryInfo = notesOfDeliveryInfo + "_" + uniqueSuffix;
        }

        // Find and click on Add Delivery Information button
        UiObject2 addPickupInfoButton = CommonActions.getTheObjectOrFail(By
                .text("Add Delivery Information")
                .clazz("android.widget.Button"), currDevice);
        addPickupInfoButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

        // Find and fill Name field
        UiObject2 nameOFDeliveryInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/nameInput"), currDevice);
        nameOFDeliveryInfoField.setText(nameOfDeliveryInfo);

        // Find and fill contact name field
        UiObject2 contactNameOfDeliveryInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/contactInput"), currDevice);
        contactNameOfDeliveryInfoField.setText(contactNameOfDeliveryInfo);

        // Find and fill Address field
        UiObject2 addressOfDeliveryInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/addressInput"), currDevice);
        addressOfDeliveryInfoField.setText(addressOfDeliveryInfo);

        // Scroll down to get access to other fields
        UiScrollable pickupInfoForm = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView")
                .scrollable(true));
        pickupInfoForm.scrollForward();

        // Find and fill city field
        UiObject2 cityOfDeliveryInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/cityInput"), currDevice);
        cityOfDeliveryInfoField.setText(cityOfDeliveryInfo);

        // Find and fill state field
        UiObject2 stateOfDeliveryInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/stateInput"), currDevice);
        stateOfDeliveryInfoField.setText(stateOfDeliveryInfo);

        // Find and fill zip code field
        UiObject2 zipCodeOfDeliveryInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/zipInput"), currDevice);
        zipCodeOfDeliveryInfoField.setText(zipCodeOfDeliveryInfo);

        // Scroll down to get access to other fields
        pickupInfoForm.scrollForward();

        // Find and fill phone number field
        UiObject2 phoneNumberOfDeliveryInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/phoneInput"), currDevice);
        phoneNumberOfDeliveryInfoField.setText(phoneNumberOfDeliveryInfo);

        // Find and fill notes field
        UiObject2 notesOfDeliveryInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/notesInput"), currDevice);
        notesOfDeliveryInfoField.setText(notesOfDeliveryInfo);

        // Find and click on Save link
        UiObject2 saveButton = CommonActions.getTheObjectOrFail(By
                .text("Save")
                .res("com.mysuperdispatch.android:id/action_save")
                .clazz("android.widget.TextView"), currDevice);
        saveButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

    }
    // End of enterDeliveryInfoData


    // Enter the customer/shipper info data
    public static void enterShipperOrCustomerInfoData(String nameOfCustomerInfo, String contactNameOfCustomerInfo, String addressOfCustomerInfo, String cityOfCustomerInfo, String stateOfCustomerInfo, String zipCodeOfCustomerInfo, String phoneNumberOfCustomerInfo, String emailOfCustomerInfo, String faxOfCustomerInfo, Boolean addUniqueSuffixToValues, Integer delayBetweenInterfaces, UiDevice currDevice) throws InterruptedException, UiObjectNotFoundException {
        if (addUniqueSuffixToValues) {
            String uniqueSuffix = System.currentTimeMillis() + "";
            nameOfCustomerInfo = nameOfCustomerInfo + "_" + uniqueSuffix;
            contactNameOfCustomerInfo = contactNameOfCustomerInfo + "_" + uniqueSuffix;
            addressOfCustomerInfo = addressOfCustomerInfo + "_" + uniqueSuffix;
        }

        // Find and click on Add Delivery Information button
        UiObject2 addPickupInfoButton = CommonActions.getTheObjectOrFail(By
                .text("Add Delivery Information")
                .clazz("android.widget.Button"), currDevice);
        addPickupInfoButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

        // Find and fill Name field
        UiObject2 nameField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/nameInput"), currDevice);
        nameField.setText(nameOfCustomerInfo);

        // Find and fill contact name field
        UiObject2 contactNameField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/contactInput"), currDevice);
        contactNameField.setText(contactNameOfCustomerInfo);

        // Find and fill Address field
        UiObject2 addressOfCustomerInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/addressInput"), currDevice);
        addressOfCustomerInfoField.setText(addressOfCustomerInfo);

        // Scroll down to get access to other fields
        UiScrollable pickupInfoForm = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView")
                .scrollable(true));
        pickupInfoForm.scrollForward();

        // Find and fill city field
        UiObject2 cityOfCustomerInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/cityInput"), currDevice);
        cityOfCustomerInfoField.setText(cityOfCustomerInfo);

        // Find and fill state field
        UiObject2 stateOfCustomerInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/stateInput"), currDevice);
        stateOfCustomerInfoField.setText(stateOfCustomerInfo);

        // Find and fill zip code field
        UiObject2 zipCodeOfCustomerInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/zipInput"), currDevice);
        zipCodeOfCustomerInfoField.setText(zipCodeOfCustomerInfo);

        // Scroll down to get access to other fields
        pickupInfoForm.scrollForward();

        // Find and fill phone number field
        UiObject2 phoneNumberOfCustomerInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/phoneInput"), currDevice);
        phoneNumberOfCustomerInfoField.setText(phoneNumberOfCustomerInfo);

        // Find and fill notes field
        UiObject2 emailOfCustomerInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/notesInput"), currDevice);
        emailOfCustomerInfoField.setText(emailOfCustomerInfo);

        // Find and fill fax field
        UiObject2 faxOfCustomerInfoField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/notesInput"), currDevice);
        faxOfCustomerInfoField.setText(faxOfCustomerInfo);

        // Find and click on Save link
        UiObject2 saveButton = CommonActions.getTheObjectOrFail(By
                .text("Save")
                .res("com.mysuperdispatch.android:id/action_save")
                .clazz("android.widget.TextView"), currDevice);
        saveButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);
    }

}
