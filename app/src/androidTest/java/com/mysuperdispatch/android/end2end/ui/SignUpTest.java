package com.mysuperdispatch.android.end2end.ui;

import android.support.test.filters.SdkSuppress;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObjectNotFoundException;

import com.mysuperdispatch.android.end2end.action.CommonActions;
import com.mysuperdispatch.android.end2end.action.LoginActions;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.mysuperdispatch.android.end2end.action.SignUpActions.signUpNewUser;


/**
 * Created by yuriy on 06.04.17.
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 21)
public class SignUpTest extends BaseTest {

    //Define current test environment
    private UiDevice theDevice;



    @Before
    public void setupTheEnvironment() {
        theDevice = CommonActions.getCurrentUIDevice();
        assertNotNull(theDevice);
        theDevice.pressHome();

    }

    @Test
    public void testSignUp() throws UiObjectNotFoundException, InterruptedException {
        // Start the application
        CommonActions.launchApplication("com.mysuperdispatch.android");

        // Perform Sign Up as Owner Operator
        signUpNewUser("Owner Operator", "OwOpName OwOpSurname", "owop@somehost.com", "Ownoppass", "+998971234567", " Yuriy Test OwOp Company", "Accept & Sign Up", 2, true, 750, theDevice);
        //signUpNewUser("Owner Operator", "OwOpName OwOpSurname", "owop@somehost.com", "?0wO*p@s$()#;'%~</br>],.\\:", "+998971234567", "OwOp Company", "Accept & Sign Up", 0, true, 750, theDevice);
        // Log out after Sign Up
        LoginActions.logout(theDevice);

        // Perform Sign Up as Fleet ...
        signUpNewUser("Fleet", "FleetName FleetSurname", "fleet@somehost.com", "fleetpAss", "+998971234567", "Fleet Company", "Accept & Sign Up", 4, true, 750, theDevice);
        //signUpNewUser("Fleet", "FleetName FleetSurname", "fleet@somehost.com", "?fleet*p@s$()#;'%~</br>],.\\:", "+998971234567", "Fleet Company", "Accept & Sign Up", 0, true, 750, theDevice);
        // Log out after Sign Up
        //LoginActions.logout(theDevice);

        // Perform Sign Up as Broker/Shipper, unique data
        //signUpNewUserWithCompanyInfo("Broker/Shipper", "BrokerName", "BrokerSurname", "broker@somehost.com", "", "+998971234569", "BrokerDiscount123", "BrokerCompany", "", "", "", "", "", "", "", "", "", "", "", "Accept & Sign Up", true, 1005, theDevice);

        // Sign Up cancel on confirmation as Broker/Shipper
        //signUpNewUser("Broker/Shipper", "BrokerName", "BrokerSurname", "broker@somehost.com", "", "+998971234569", "Broker Company", "BrokerDiscount123", "Cancel", true);
    }
}
