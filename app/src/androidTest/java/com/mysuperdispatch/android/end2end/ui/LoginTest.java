package com.mysuperdispatch.android.end2end.ui;


import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObjectNotFoundException;

import com.mysuperdispatch.android.end2end.action.CommonActions;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.mysuperdispatch.android.end2end.action.LoginActions.login;
import static com.mysuperdispatch.android.end2end.action.LoginActions.logout;


/**
 * Created by yuriy on 06.04.17.
 */

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 21)
public class LoginTest extends BaseTest {

    //Define current test environment.
    private UiDevice theDevice;

    @Before
    public void setupTheEnvironment() {
        theDevice = CommonActions.getCurrentUIDevice();
        assertNotNull(theDevice);
        theDevice.pressHome();
    }

    @Test
    public void testLoginLogout() throws UiObjectNotFoundException, InterruptedException {

        // Launch application
        CommonActions.launchApplication("com.mysuperdispatch.android");

        // Test Login using password authentification
        login("yurmanok@mail.ru", "yuriy", false, 1000, theDevice);
        logout(theDevice);

        //login("yuf`~!#$%^&*-=+|'.?/1@somehost.com", "`~!@#$%^&*()-=+/|?", 1200, theDevice);
        //logout(theDevice);
    }
}
