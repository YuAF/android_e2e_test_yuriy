package com.mysuperdispatch.android.end2end.action;

import android.app.UiAutomation;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.StaleObjectException;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;


/**
 * Created by yuriy on 26.07.17.
 */

public class HoursOfServiceActions {

    private static final String locationInputResourceID = "com.mysuperdispatch.android:id/locationInput";
    private static final String violationOfElevenHourDriving = "You reached 11 hour driving limit";
    private static final String violationOfFourteenHourDrivingWindow = "You reached 14 hour shift limit";
    private static final String violationOfThirtyMinutesBreakRequiredForDrivingAfterEightHoursOnDuty = "30 minute break required";
    private static final String violationOfSeventyHoursDrivingWindow = "You reached 70 hour driving limit";


    public static void selectDutyStatus(String dutyStatusName, UiDevice currentDevice) throws InterruptedException {

        CommonActions.getTheObjectOrFail(By.clazz("android.widget.RelativeLayout").hasChild(By.text(dutyStatusName)), currentDevice).click();
    }


    public static void changeDutyStatus(String buttonToSelectInHoS, String buttonToSelectInChange, String locationValue, String notesValue, UiDevice currDevice) throws  InterruptedException {

        selectDutyStatus(buttonToSelectInHoS, currDevice);
        ChangeDutyStatusActions.fillLocationField(locationValue, currDevice);
        ChangeDutyStatusActions.selectDutyStatus(buttonToSelectInChange, currDevice);
        ChangeDutyStatusActions.fillNotesField(notesValue, currDevice);
        ChangeDutyStatusActions.saveDutyStatusChanges(currDevice);
    }

    public static UiObject2 getDailyRecordByNumber(int recordRowNumber, UiDevice currDevice) throws InterruptedException {

        int rowIndex = 0;
        switch (recordRowNumber) {
            case 1:
                rowIndex = 1;
                break;
            case 2:
                rowIndex = 3;
                break;
            case 3:
                rowIndex = 5;
                break;
            case 4:
                rowIndex = 7;
                break;
            case 5:
                rowIndex = 9;
                break;
            case 6:
                rowIndex = 11;
                break;
            case 7:
                rowIndex = 13;
                break;
            case 8:
                rowIndex = 15;
                break;
        }

        UiObject2 dailyLogRecord = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.ScrollView"), currDevice)
                .getChildren().get(0).getChildren().get(2).getChildren().get(rowIndex);
        return dailyLogRecord;
    }

    // Open by the defined number of the record row, counting from top to bottom
    public static void openDailyLogRecordByRowNumber(int recordRowNumber, UiDevice currDevice) throws InterruptedException {

        getDailyRecordByNumber(recordRowNumber, currDevice).click();
    }

    public static void scrollHoSInterface(int stepsToScroll, UiDevice currentDevice) throws InterruptedException, UiObjectNotFoundException {

        CommonActions.scrollTheInterface(stepsToScroll, currentDevice);
    }

    public static Boolean isDrivingLeftValueEquals(String hoursLeft, String minutesLeft, UiDevice currentDevice) throws InterruptedException {

        String checkValue = hoursLeft + "h " + minutesLeft + "m left";
        UiObject2 drivingTimeLeftLabel = CommonActions.getTheObjectOrFail(By.text("Driving"), currentDevice).getParent().getChildren().get(2);
        if (drivingTimeLeftLabel.getText() == checkValue) {
            return true;
        }
        else {
            return false;
        }
    }

    public static Boolean isTheDutyStatusButtonSelected(String dutyStatusName, UiDevice currentDevice) throws  InterruptedException{

        UiObject2 selectedDutyStatusButton = CommonActions.getTheObjectOrFail(By.clazz("android.widget.RelativeLayout").hasChild(By.text(dutyStatusName)), currentDevice);

        if (selectedDutyStatusButton.isSelected()) {
            return true;
        }
        else {
            return false;
        }
    }

    public static Boolean isViolationIconDisplayedForTheDailyLogRecord(int recordNumber, UiDevice currentDevice) throws InterruptedException {
        if (getDailyRecordByNumber(recordNumber, currentDevice).getChildCount() > 1) {
            return true;
        }
        else {
            return false;
        }
    }


    // - - - - - - - - - - //
    // The class for acting with Change Duty Status interface
    public static class ChangeDutyStatusActions {

        private static String notesInputResourceID = "com.mysuperdispatch.android:id/notesInput";

        public static void selectDutyStatus(String dutyStatusName, UiDevice currentDevice) throws InterruptedException {

            CommonActions.getTheObjectOrFail(By.clazz("android.widget.RelativeLayout").hasChild(By.text(dutyStatusName)), currentDevice).click();
        }

        public static void fillLocationField(String textValue, UiDevice currentDevice) throws InterruptedException {

            //CommonActions.getTheObjectOrFail(By.res(locationInputResourceID), currentDevice).setText(textValue);
            CommonActions.fillByResourceID(locationInputResourceID, textValue, currentDevice);
        }

        public static void setLocationAsCurrent(UiDevice currentDevice) throws InterruptedException {

            //CommonActions.getTheObjectOrFail(By.res(locationInputResourceID), currentDevice).getParent().getParent().getParent().getChildren().get(1).click();
            CommonActions.performSetCurrentLocation(currentDevice);
        }

        public static void fillNotesField(String textValue, UiDevice currentDevice) throws InterruptedException {

            getNotesInput(currentDevice).setText(textValue);
        }

        public static void saveDutyStatusChanges(UiDevice currentDevice) throws InterruptedException {

            CommonActions.getTheObjectOrFail(By.clazz("android.widget.Button").text("Save"), currentDevice).click();
        }


        public static Boolean isLocationValueEquals(String valueToCheck, UiDevice currentDevice) throws  InterruptedException {

            UiObject2 locationField = CommonActions.getTheObjectOrFail(By.res(locationInputResourceID), currentDevice);
            if (locationField.getText() == valueToCheck) {
                return true;
            }
            else {
                return false;
            }
        }

        public static UiObject2 getNotesInput(UiDevice currDevice) throws  InterruptedException {

            return CommonActions.getTheObjectOrFail(By.res(notesInputResourceID), currDevice);

        }

        public static Boolean isNotesValueEquals(String valueToCheck, UiDevice currentDevice) throws  InterruptedException {

            UiObject2 notesInput = getNotesInput(currentDevice);
            if (notesInput.getText() == valueToCheck) {
                return true;
            }
            else {
                return false;
            }
        }
    }



    // - - - - - - - - - - //
    // The class for acting in Daily Log interface
    public static class DailyLogActions {

        public static void addDutyStatusRecord(int startTimeHour, int startTimeMinute, String startTimeAMOrPM, int endTimeHour, int endTimeMinute, String endTimeAMOrPM, String dutyStatusButtonText, UiDevice currDevice) throws InterruptedException, UiObjectNotFoundException {

            openAddNewRecordInterface(currDevice);
            //String startTimeText = Integer.toString(startTimeHour) + ":" + Integer.toString(startTimeMinute) + " " + startTimeAMOrPM;
            //String endTimeText = Integer.toString(endTimeHour) + ":" + Integer.toString(endTimeMinute) + " " + endTimeAMOrPM;
            DutyStatusActivityRecordActions.openClockInterface("Start Time", currDevice);
            DutyStatusActivityRecordActions.ClockActions.setTheTime(startTimeHour, startTimeMinute, startTimeAMOrPM, "OK", currDevice);
            DutyStatusActivityRecordActions.openClockInterface("End Time", currDevice);
            DutyStatusActivityRecordActions.ClockActions.setTheTime(endTimeHour, endTimeMinute, endTimeAMOrPM, "OK", currDevice);
            DutyStatusActivityRecordActions.selectDutyStatusButton(dutyStatusButtonText, currDevice);
            DutyStatusActivityRecordActions.saveChanges(currDevice);
        }

        public static void editDutyStatusRecord(int recordNumber, int startTimeHour, int startTimeMinute, String startTimeAMOrPM, int endTimeHour, int endTimeMinute, String endTimeAMOrPM, String dutyStatusButtonText, UiDevice currDevice) throws InterruptedException, UiObjectNotFoundException {

            //openAddNewRecordInterface(currDevice);
            openDutyStatusActivityRecord(recordNumber, currDevice);
            String startTimeText = Integer.toString(startTimeHour) + ":" + Integer.toString(startTimeMinute) + " " + startTimeAMOrPM;
            String endTimeText = Integer.toString(endTimeHour) + ":" + Integer.toString(endTimeMinute) + " " + endTimeAMOrPM;
            DutyStatusActivityRecordActions.openClockInterface("Start Time", currDevice);
            DutyStatusActivityRecordActions.ClockActions.setTheTime(startTimeHour, startTimeMinute, startTimeAMOrPM, "OK", currDevice);
            DutyStatusActivityRecordActions.openClockInterface("End Time", currDevice);
            DutyStatusActivityRecordActions.ClockActions.setTheTime(endTimeHour, endTimeMinute, endTimeAMOrPM, "OK", currDevice);
            DutyStatusActivityRecordActions.selectDutyStatusButton(dutyStatusButtonText, currDevice);
            DutyStatusActivityRecordActions.saveChanges(currDevice);
        }

        public static UiObject2 getDutyStatusActivityRecordByNumber(int recordRowNumber, UiDevice currentDevice) throws InterruptedException {

            int rowIndex = 0;
            switch (recordRowNumber) {
                case 1:
                    rowIndex = 1;
                    break;
                case 2:
                    rowIndex = 3;
                    break;
                case 3:
                    rowIndex = 5;
                    break;
                case 4:
                    rowIndex = 7;
                    break;
                case 5:
                    rowIndex = 9;
                    break;
                case 6:
                    rowIndex = 11;
                    break;
                case 7:
                    rowIndex = 13;
                    break;
                case 8:
                    rowIndex = 15;
                    break;
            }

            UiObject2 dutyStatusActivityLogRecord = CommonActions.getTheObjectOrFail(By
                    .clazz("android.widget.ScrollView"), currentDevice)
                    .getChildren().get(0).getChildren().get(3).getChildren().get(rowIndex);
            return dutyStatusActivityLogRecord;
        }

        public static void openDutyStatusActivityRecord(int recordRowNumber, UiDevice currentDevice) throws InterruptedException {

            getDutyStatusActivityRecordByNumber(recordRowNumber, currentDevice).click();
        }


        public static void openAddNewRecordInterface(UiDevice currDevice) throws InterruptedException, UiObjectNotFoundException {

            // if the Daily Log interface is scrollable (contains plenty records)
            UiObject2 theScrollView = CommonActions.getTheObjectOrFail(By.clazz("android.widget.ScrollView"), currDevice);
            Boolean isTheInterfaceScrollable = theScrollView.isScrollable();
            if (isTheInterfaceScrollable)
            {
                scrollHoSInterface(999, currDevice);
            }
            theScrollView.recycle();
            CommonActions.tapTheButton("Add New Record", currDevice);
        }



        // - - - - - - - - - - //
        // The class for acting in Duty Status Activity Record interface -
        // i.e Add New Record interface, Edit Record interface
        public static class DutyStatusActivityRecordActions {


            // The class for acting in Clock modal interface
            public static class ClockActions {

                public static void setTheTime(int hourOfTime, int minuteOfTime, String dayAMOrPM, String buttonToClick, UiDevice currDevice) throws InterruptedException {

                    // Select a hour
                    CommonActions.selectClockSign(hourOfTime, currDevice);

                    // Select a minute
                    CommonActions.selectClockSign(minuteOfTime, currDevice);

                    // Select AM or PM
                    CommonActions.tapTheRadioButton(dayAMOrPM, currDevice);
                    // Click OK or Cancel
                    CommonActions.tapTheButton(buttonToClick, currDevice);
                }

            }

            public static void openClockInterface(String theClockLable, UiDevice currDevice) throws InterruptedException {

                CommonActions.tapItemUnderTheLabel(theClockLable, currDevice);
            }

            public static void openClockInterfaceIf(String theClockLable, UiDevice currDevice) throws InterruptedException {

                CommonActions.tapItemUnderTheLabel(theClockLable, currDevice);
            }

            public static void selectDutyStatusButton(String buttonText, UiDevice currDevice) throws InterruptedException {

                //CommonActions.getTheObjectOrFail(By.clazz("android.widget.Button").text(buttonText), currDevice).click();
                CommonActions.tapTheButton(buttonText, currDevice);
            }

            public static void fillLocationField(String textValue, UiDevice currentDevice) throws InterruptedException {

                //CommonActions.getTheObjectOrFail(By.res(locationInputResourceID), currentDevice).setText(textValue);
                CommonActions.fillByResourceID(locationInputResourceID, textValue, currentDevice);
            }

            public static void setLocationAsCurrent(UiDevice currentDevice) throws InterruptedException {

                CommonActions.performSetCurrentLocation(currentDevice);
            }

            public static UiObject2 getNotesInput(UiDevice currDevice) throws InterruptedException {
                return CommonActions.getTheObjectOrFail(By.clazz("android.widget.ScrollView"), currDevice).getChildren().get(0).getChildren().get(4).getChildren().get(0).getChildren().get(0);
            }

            public static void fillNotesField(String textValue, UiDevice currentDevice) throws InterruptedException {

                getNotesInput(currentDevice).setText(textValue);
            }

            public static void saveChanges(UiDevice currDevice) throws InterruptedException {
                CommonActions.getTheObjectOrFail(By
                        .clazz("android.widget.TextView")
                        .res("com.mysuperdispatch.android:id/action_save")
                        .text("SAVE"), currDevice).click();
            }

            public static Boolean isLocationValueEquals(String valueToCheck, UiDevice currentDevice) throws  InterruptedException {

                UiObject2 locationField = CommonActions.getTheObjectOrFail(By.res(locationInputResourceID), currentDevice);
                if (locationField.getText() == valueToCheck) {
                    return true;
                }
                else {
                    return false;
                }
            }

            public static Boolean isNotesValueEquals(String valueToCheck, UiDevice currentDevice) throws  InterruptedException {

                UiObject2 notesInput = getNotesInput(currentDevice);
                if (notesInput.getText() == valueToCheck) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }
        // End of DutyStatusActivityRecordActionsClass

        public static Boolean isViolationDisplayed(String violationText, UiDevice currentDevice) throws InterruptedException {

            if (CommonActions.getTheObjectOrNull(By.clazz("android.widget.TextView").text(violationText), currentDevice) != null) {
                return true;
            }
            else {
                return false;
            }
        }

        public static Boolean isViolationOfElevenHoursDrivingDisplayed(UiDevice currentDevice) throws InterruptedException {

            if (CommonActions.getTheObjectOrNull(By.clazz("android.widget.TextView").text(violationOfElevenHourDriving), currentDevice) != null) {
                return true;
            }
            else {
                return false;
            }
        }

        public static Boolean isViolationOfFourteenHoursDrivingWindowDisplayed(UiDevice currentDevice) throws InterruptedException {

            if (CommonActions.getTheObjectOrNull(By.clazz("android.widget.TextView").text(violationOfFourteenHourDrivingWindow), currentDevice) != null) {
                return true;
            }
            else {
                return false;
            }
        }

        public static Boolean isViolationOfEightHoursDrivingWindowDisplayed(UiDevice currentDevice) throws InterruptedException {

            if (CommonActions.getTheObjectOrNull(By.clazz("android.widget.TextView").text(violationOfThirtyMinutesBreakRequiredForDrivingAfterEightHoursOnDuty), currentDevice) != null) {
                return true;
            }
            else {
                return false;
            }
        }

        public static Boolean isViolationOfSeventyHoursDrivingWindowDisplayed(UiDevice currentDevice) throws InterruptedException {
            if (CommonActions.getTheObjectOrNull(By.clazz("android.widget.TextView").text(violationOfSeventyHoursDrivingWindow), currentDevice) != null) {
                return true;
            }
            else {
                return false;
            }
        }

        public static Boolean isViolationCountMatches(int violationsCount, UiDevice currentDevice) throws InterruptedException {

            // The Violations section appears, when there is at least one Violation is displayed.
            // Otherwise, the section is not the Violations
            // (the Duty Status Activity Records section appears in this position instead).
            // Check if appropriate section of the interface is the Violations section (in certain position has a field with text)
            UiObject2 childZeroOfThirdLinear = CommonActions.getTheObjectOrFail(By.clazz("android.widget.ScrollView"), currentDevice).getChildren().get(0).getChildren().get(3).getChildren().get(0);
            String textInControl = childZeroOfThirdLinear.getText();
            if (textInControl != null && textInControl != "") {
                // If the section is Violations, perform checking of the items count
                if (childZeroOfThirdLinear.getParent().getChildCount() == violationsCount) {
                    childZeroOfThirdLinear.recycle();
                    return true;
                }
                else {
                    childZeroOfThirdLinear.recycle();
                    return false;
                }
            }
            // If the section is not the Violation section
            else {
                if (violationsCount == 0)
                {
                    childZeroOfThirdLinear.recycle();
                    return true;
                }
                else {
                    childZeroOfThirdLinear.recycle();
                    return false;
                }
            }
        }

    }
}
