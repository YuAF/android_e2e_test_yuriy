package com.mysuperdispatch.android.end2end.action;

import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isEnabled;
import static android.support.test.espresso.matcher.ViewMatchers.withHint;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;

/**
 * Created by bahrom on 5/3/17.
 */

// Sign Up method to use in tests
public class SignUpActions {

    public static void signUpNewUser(String roleName, String fullName, String emailAddress, String passWordOfUser, String phoneNumber, String companyName, String buttonTextForConfirmationDialog, int SkipPageNumber, Boolean addUniqueSuffixToValues, int delayBetweenInterfaces, UiDevice currentDevice) throws UiObjectNotFoundException, InterruptedException {

        // If it is requested, add unique suffix to first name, last name, email, PassWord - for example, to avoid duplicates validation during test
        if (addUniqueSuffixToValues) {
            String uniqueSuffix = System.currentTimeMillis() + "";
            fullName = fullName + "_" + uniqueSuffix;
            emailAddress = uniqueSuffix + "_" + emailAddress;
            passWordOfUser = passWordOfUser + "_" + uniqueSuffix;
            companyName = companyName + "_" + uniqueSuffix;
        }

        // set suffix to recognize test data in Production environment
        fullName = fullName + "_" + "*****";
        companyName = companyName + "_" + "*****";

        // Find and click on Get Started button
        UiObject2 GetStartedButton = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.Button")
                .text("Get Started"), currentDevice);
        GetStartedButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

        // onView(withText("Get Started")).perform(click());

        // Find email field and enter text
        UiObject2 EmailField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/emailInput"), currentDevice);
        EmailField.setText(emailAddress);

        //onView(withHint("Email")).perform(replaceText(emailAddress));

        // Find and click on Next button
        UiObject2 NextButton = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.Button")
                .text("Next"), currentDevice);
        NextButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

        // Find layout containing text field with specified value and click on the layout
        UiObject2 RoleLayout = CommonActions.getTheObjectOrFail(By.text(roleName), currentDevice);
        RoleLayout.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

        // Set presentation pages count variable regarding specified Role Name
        Integer PagesCount = 0;

        switch (roleName) {
            case "Owner Operator":
                PagesCount = 3;
                break;
            case "Fleet":
                PagesCount = 5;
                break;
            // commented, see https://trello.com/c/rppCIF1s/449-remove-the-broker-shipper-sign-up-option-from-the-mobile-apps
            /*
            case "Broker/Shipper":
                PagesCount = 5;
                break;
            */
        }

        // List presentation pages
        String presentationButtonCaption = "";
        for (int c = 1; c <= PagesCount; c++) {

            // If Skip functionality should be tested in current page, find and tap on Skip button
            if (c == SkipPageNumber) {
                UiObject2 skipButton = CommonActions.getTheObjectOrFail(By
                        .text("Skip")
                        .clazz("android.widget.Button"), currentDevice);
                skipButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces);
                break;
            }

            // Define text on button regarding last step or not and Role Name
            if (c < PagesCount) {
                presentationButtonCaption = "Next";
            }

            else {
                // commented, see https://trello.com/c/rppCIF1s/449-remove-the-broker-shipper-sign-up-option-from-the-mobile-apps
                /*
                if (roleName.equals("Broker/Shipper")) {
                    presentationButtonCaption = "Yes, I'm Interested";
                } else {
                    presentationButtonCaption = "Sign Up";
                }
                */
                presentationButtonCaption = "Sign Up";
            }

            // Find and click on the button with Next or SIGN UP caption
            UiObject2 NextOrSignUpButton = CommonActions.getTheObjectOrFail(By
                    .clazz("android.widget.Button")
                    .text(presentationButtonCaption), currentDevice);
            NextOrSignUpButton.clickAndWait(Until.newWindow(), 1000);
        }
        // end of the presentation pages handler cycle

        // Filling opened Sign Up form

        // Find and fill email field
        UiObject2 EmailFieldSignUp = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/emailInput"), currentDevice);
        EmailFieldSignUp.clear();
        EmailFieldSignUp.setText(emailAddress);

        // Find and enter value in Full Name field
        UiObject2 fullNameField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/fullNameInput"), currentDevice);
        fullNameField.setText(fullName);

        // Find and enter value in last name field.
        // Commented, because for now - Full Name field is used instead of two name fields
        // see https://trello.com/c/mBkgzEWu/138-review-as-a-new-user-i-do-not-want-to-enter-the-company-information-during-sign-up
        /*
        UiObject2 lastNameField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/lastNameT"), currentDevice);
        lastNameField.setText(lastName);
        */


        // If role specified as broker/shipper, find and fill company name field
        // Commented, because possibility of selecting Broker/Shipper role is removed from Sign Up process within mobile apps
        // see https://trello.com/c/rppCIF1s/449-remove-the-broker-shipper-sign-up-option-from-the-mobile-apps
        /*
        if (roleName.equals("Broker/Shipper")) {
            UiObject2 companyNameFieldForBroker = CommonActions.getTheObjectOrFail(By
                    .clazz("android.widget.EditText")
                    .res("com.mysuperdispatch.android:id/companyNameT"), currentDevice);
            companyNameFieldForBroker.setText(companyName);
        }
        */

        // If role specified as owner operator or fleet, find and fill PassWord field
        if (roleName.equals("Owner Operator") || roleName.equals("Fleet")) {
            UiObject2 passwordField = CommonActions.getTheObjectOrFail(By
                    .clazz("android.widget.EditText")
                    .res("com.mysuperdispatch.android:id/passwordInput"), currentDevice);
            passwordField.setText(passWordOfUser);
        }


        // Scroll down to get access to fields in bottom
        /*
        UiScrollable userSignUpForm = new UiScrollable(new UiSelector()
                .className("android.widget.ScrollView")
                .scrollable(true));
        userSignUpForm.scrollForward();*/

        // Find and fill phone number field
        UiObject2 phoneNumberField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/phoneInput"), currentDevice);
        phoneNumberField.setText(phoneNumber);

        // This need to hide virtual keyboard to get access to discount code field
        currentDevice.pressBack();

        // Find and fill discount code field
        // Commented, because possibility to enter discount code is removed from Sign Up process within mobile apps
        /*
        UiObject2 DiscountCodeField = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.EditText")
                .res("com.mysuperdispatch.android:id/discountCodeInput"), currentDevice);
        DiscountCodeField.setText(discountCode);
        */

        // Find and fill company name field
        UiObject2 companyNameField = CommonActions.getTheObjectOrFail(By
                .res("com.mysuperdispatch.android:id/companyNameInput")
                .clazz("android.widget.EditText"), currentDevice);
        companyNameField.setText(companyName);

        // Define text of link to be clicked regarding specified role, then find and click to the button
        //String LinkText = "DONE";
        String ButtonText = "Sign Up";

        /* commented, see https://trello.com/c/rppCIF1s/449-remove-the-broker-shipper-sign-up-option-from-the-mobile-apps
        if (roleName.equals("Broker/Shipper")) {
            LinkText = "Done";
        }

        if (roleName.equals("Owner Operator") || roleName.equals("Fleet")) {
            LinkText = "Next";
        }
        */

        UiObject2 NextOrDoneLinkWithinSignUpForm = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.Button")
                .text(ButtonText), currentDevice);
        NextOrDoneLinkWithinSignUpForm.clickAndWait(Until.newWindow(), 5000);

        // Answer the Sign Up confirmation dialog
        UiObject2 confirmationDialogButton = CommonActions.getTheObjectOrFail(By
                .clazz("android.widget.Button")
                .text(buttonTextForConfirmationDialog), currentDevice);
        confirmationDialogButton.clickAndWait(Until.newWindow(), delayBetweenInterfaces * 4);

        // Do next steps regarding specified answer and selected role
        if (buttonTextForConfirmationDialog.equals("Accept & Sign Up")) {
            // For broker/shipper, make sure that registration was successful
            // commented, see https://trello.com/c/rppCIF1s/449-remove-the-broker-shipper-sign-up-option-from-the-mobile-apps
            /*
            if (roleName.equals("Broker/Shipper")) {
                // Find Done button, click on it
                UiObject2 ButtonDone = CommonActions.getTheObjectOrFail(By
                        .clazz("android.widget.Button")
                        .text("Done"), currentDevice);
                ButtonDone.clickAndWait(Until.newWindow(), delayBetweenInterfaces);

                // Find Get Started button
                UiObject2 ButtonGetStartedAfterSignUp = CommonActions.getTheObjectOrFail(By
                        .clazz("android.widget.Button")
                        .text("GET STARTED"), currentDevice);
                //assertNotNull(ButtonGetStartedAfterSignUp);
            }
            */

            // For Owner Operator or Fleet, verify that new user is logged in
            if (roleName.equals("Owner Operator") || roleName.equals("Fleet")) {
                // Close all notifications, if appeared
                //CommonActions.closeNotifications(currentDevice, delayBetweenInterfaces);
                CommonActions.closeIntercomNotifications(currentDevice, delayBetweenInterfaces);
                // After handling known notifucations, verify that user is logged in.
                CommonActions.verifyThatLoggedIn(currentDevice);
            }

        }

        // For Sign Up confirmation cancel test, verify Sign Up form by clickable Sign Up button
        if (buttonTextForConfirmationDialog.equals("Cancel")) {
            UiObject2 DoneButtonOnSignUpForm = CommonActions.getTheObjectOrFail(By
                    .text("Sign Up")
                    .clazz("android.widget.Button")
                    .clickable(true), currentDevice);
            //assertNotNull(DoneButtonOnSignUpForm);
        }
    }
}
