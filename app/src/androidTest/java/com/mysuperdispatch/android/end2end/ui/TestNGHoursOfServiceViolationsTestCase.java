package com.mysuperdispatch.android.end2end.ui;


import static org.testng.AssertJUnit.*;
import org.testng.annotations.*;

import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObjectNotFoundException;

import com.mysuperdispatch.android.end2end.action.CommonActions;
import com.mysuperdispatch.android.end2end.action.HoursOfServiceActions;
import com.mysuperdispatch.android.end2end.action.MainActions;


import java.io.File;

import static com.mysuperdispatch.android.end2end.action.LoginActions.login;
import static com.mysuperdispatch.android.end2end.action.LoginActions.logout;
//import static org.junit.Assert.assertNotNull;
//import static org.junit.Assert.assertTrue;

/**
 * Created by yuriy on 26.07.17.
 */



//@SdkSuppress(minSdkVersion = 21)
public class TestNGHoursOfServiceViolationsTestCase {

    private static UiDevice theDevice;
    private static int delayBetweenInterfaces;
    private static String screeshotPatternDailyLog;
    private static String screeshotPatternAddNewRecord;
    private static String violationOfElevenHoursDrivingIsNotDisplayed = "The 11 hours+ driving violation is not displayed!";
    private static String violationOfFourteenHoursDrivingWindowIsNotDisplayed = "The 14 hours+ violation is not displayed!";
    private static String violationOfEightHoursDrivingWindowIsNotDisplayed = "The 30 minutes break after 8 hours driving violation is not displayed!";
    private static String violationOfSeventyHoursDrivingWindowIsNotDisplayed = "The 70 hours+ violation is not displayed!";
    private static String violationsCountIsIncorrect = "Violations count is incorrect!";
    private static String violationIconIsNotDisplayedForEightDayHistoryRecord = "The Violation icon is not displayed for appropriate record in 8 Day History section!";
    private static String unexpectedDisplayingOfViolationIcon = "The Violation icon is displayed unexpectedly in 8 Day History section for the record!";
    private static String AM = "AM";
    private static String PM = "PM";
    private static String Driving = "D";
    private static String OnDuty = "ON";
    private static String SleeperBerth = "SB";
    private static String OffDuty = "OFF";


    public static void checkThatViolationIconIsNOTDisplayedForTheRecord(int recordNumber, UiDevice currentDevice) throws InterruptedException {
        assertTrue(unexpectedDisplayingOfViolationIcon + " (record #" + recordNumber + ")", !HoursOfServiceActions.isViolationIconDisplayedForTheDailyLogRecord(recordNumber, currentDevice));
    }

    public static void checkThatViolationIconIsDISPLAYEDForTheRecord(int recordNumber, UiDevice currentDevice) throws InterruptedException {
        assertTrue(violationIconIsNotDisplayedForEightDayHistoryRecord + " (record #" + recordNumber + ")", HoursOfServiceActions.isViolationIconDisplayedForTheDailyLogRecord(recordNumber, currentDevice));
    }

    public static void checkViolationsCountEquals(int checkCount, UiDevice currentDevice) throws InterruptedException {
        assertTrue(violationsCountIsIncorrect + " (not equal " + checkCount +")", HoursOfServiceActions.DailyLogActions.isViolationCountMatches(checkCount, currentDevice));
    }

    public static void checkViolationOfElevenHoursDrivingIsDisplayed(UiDevice currentDevice) throws InterruptedException {
        assertTrue(violationOfElevenHoursDrivingIsNotDisplayed, HoursOfServiceActions.DailyLogActions.isViolationOfElevenHoursDrivingDisplayed(currentDevice));
    }

    public static void checkViolationOfFourteenHoursDrivingWindowIsDisplayed(UiDevice currentDevice) throws InterruptedException {
        assertTrue(violationOfFourteenHoursDrivingWindowIsNotDisplayed, HoursOfServiceActions.DailyLogActions.isViolationOfFourteenHoursDrivingWindowDisplayed(currentDevice));
    }

    public static void checkViolationOfEightHoursDrivingWindowIsDisplayed(UiDevice currentDevice) throws InterruptedException {
        assertTrue(violationOfEightHoursDrivingWindowIsNotDisplayed, HoursOfServiceActions.DailyLogActions.isViolationOfEightHoursDrivingWindowDisplayed(currentDevice));
    }

    public static void checkViolationOfSeventyHoursDrivingWindowIsDisplayed(UiDevice currentDevice) throws InterruptedException {
        assertTrue(violationOfSeventyHoursDrivingWindowIsNotDisplayed, HoursOfServiceActions.DailyLogActions.isViolationOfSeventyHoursDrivingWindowDisplayed(currentDevice));
    }


    @BeforeClass
    public static void testCaseSetup() throws InterruptedException, UiObjectNotFoundException {
        theDevice = CommonActions.getCurrentUIDevice();
        assertNotNull(theDevice);
        theDevice.pressHome();
        CommonActions.launchApplication("com.mysuperdispatch.android");
        delayBetweenInterfaces = 1250;
        screeshotPatternDailyLog = "HoS_Daily_";
        screeshotPatternAddNewRecord = "HoS_AddNewRecord_";

        // Login using password authentification
        //login("yuriy@mysuperdispatch.com", "yuriy", false, 1500, theDevice);

        // Open Hours Of Service interface by tap on the tab
        MainActions.openHoSTab(theDevice);
    }

    @BeforeMethod
    public void setup()  throws InterruptedException {

        // if Main interface is not opened, perfrom Back navigation
        while (!MainActions.isMainInterfaceDisplayed(theDevice)) {
            MainActions.navigateBack(theDevice);
        }
    }

    // Testing steps described in the doc "HOS_Logbook_Examples_2015MAY_508.pdf".

    // Case #1
    // violation 1 example 4
    @Test(priority = 1)
    public void testViolationExampleFourDrivingLimitAndWindowLimit() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        MainActions.navigateBack(theDevice);
        //HoursOfServiceActions.scrollHoSInterface(999, theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 1, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(1, 0, AM, 5, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, AM, 10, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 3, 0, PM, Driving, theDevice);

        // Check that violations are displayed in the Daily Log interface
        checkViolationOfElevenHoursDrivingIsDisplayed(theDevice);
        checkViolationOfFourteenHoursDrivingWindowIsDisplayed(theDevice);

        // Check that only two violations are displayed
        //assertTrue(violationsCountIsIncorrect + " (not equal 2)", HoursOfServiceActions.DailyLogActions.isViolationCountMatches(2, theDevice));
        checkViolationsCountEquals(2, theDevice);
        MainActions.navigateBack(theDevice);

        // Check that violation item presents in the 8 Day History Log section in appropriate line.

        checkThatViolationIconIsDISPLAYEDForTheRecord(8, theDevice);
    }

    // Case #2
    // violation 2 example 5
    @Test(priority = 2)
    public void testViolationExampleFiveThirtyMinutesRestBreak() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 11, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 4, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, PM, 6, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, PM, 10, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, PM, 11, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, PM, 12, 0, AM, Driving, theDevice);

        checkViolationOfEightHoursDrivingWindowIsDisplayed(theDevice);
        checkViolationsCountEquals(1, theDevice);
        MainActions.navigateBack(theDevice);
        checkThatViolationIconIsDISPLAYEDForTheRecord(8, theDevice);
    }

    // Case #3
    // violation 3 example 8
    @Test(priority = 3)
    public void testViolationExampleEightThirtyMinutesRestBreak() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 11, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 6, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, PM, 7, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(7, 0, PM, 8, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(8, 30, PM, 12, 0, AM, Driving, theDevice);

        checkViolationOfEightHoursDrivingWindowIsDisplayed(theDevice);

        // (YuF, 20170801): There are two violations - 8h+ driving and 11h+ driving (from 11:30 PM).
        // For now, HoS exceptions (such as 16h Window and 13h Driving) are not implemented within the app.
        // Second violation may not appears, when exceptions will be implemented, so this case aslo
        // should be reviewed then.
        checkViolationsCountEquals(2, theDevice);

        MainActions.navigateBack(theDevice);
        checkThatViolationIconIsDISPLAYEDForTheRecord(8,theDevice);
    }

    // Case #4
    //violation 4 example 16
    @org.testng.annotations.Test(priority = 4)
    public void testViolationExampleSixteenTwoDriversTwoHoursOfJumpSeatCombinedWithEightHoursOfSleeperBerth() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 12, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, PM, 3, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, PM, 12, 0, AM, SleeperBerth, theDevice);

        MainActions.navigateBack(theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(1, 0, AM, 4, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, AM, 9, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 3, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(3, 0, PM, 4, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, PM, 5, 0, PM, Driving, theDevice);

        checkViolationOfFourteenHoursDrivingWindowIsDisplayed(theDevice);
        checkViolationsCountEquals(1, theDevice);

        MainActions.navigateBack(theDevice);
        checkThatViolationIconIsDISPLAYEDForTheRecord(7, theDevice);
    }

    // Case #5
    // violation 5 example 18
    @Test(priority = 5)
    public void testViolationExampleEighteenSleeperBerthUse() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 3, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, PM, 7, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(7, 0, PM, 12, 0, AM, SleeperBerth, theDevice);

        MainActions.navigateBack(theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 3, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(3, 0, AM, 7, 0, AM, Driving, theDevice);

        checkViolationOfElevenHoursDrivingIsDisplayed(theDevice);
        checkViolationsCountEquals(1, theDevice);
        MainActions.navigateBack(theDevice);
        checkThatViolationIconIsDISPLAYEDForTheRecord(7, theDevice);
    }


    // Case #6
    // Violation 6 example 20
    @Test(priority = 6)
    public void testViolationExampleTwentySleeperBerthUse() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 3, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(3, 0, PM, 11, 0, PM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, PM, 12, 0, AM, Driving, theDevice);

        MainActions.navigateBack(theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 4, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, AM, 5, 30, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(5, 30, AM, 1, 0, PM, Driving, theDevice);

        checkViolationOfElevenHoursDrivingIsDisplayed(theDevice);
        checkViolationOfFourteenHoursDrivingWindowIsDisplayed(theDevice);
        checkViolationsCountEquals(2, theDevice);

        MainActions.navigateBack(theDevice);
        checkThatViolationIconIsDISPLAYEDForTheRecord(7, theDevice);
    }

    // Case #7
    // violation 7 - 70 hour+ test case, short, not from examples doc.
    @Test(priority = 7)
    public void testViolationOfSeventyHoursDrivingWindow() throws InterruptedException, UiObjectNotFoundException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);        
        HoursOfServiceActions.openDailyLogRecordByRowNumber(6, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OnDuty, theDevice);
        MainActions.navigateBack(theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(4, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OnDuty, theDevice);
        MainActions.navigateBack(theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(2, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(2, 0, AM, 12, 0, AM, Driving, theDevice);

        checkViolationOfSeventyHoursDrivingWindowIsDisplayed(theDevice);
        //checkViolationsCountEquals(4, theDevice);
        MainActions.navigateBack(theDevice);

        checkThatViolationIconIsDISPLAYEDForTheRecord(2, theDevice);
    }

    // Case #8
    @Test(priority = 8)
    public void testExampleTwoTheTenConsecutiveHoursOffDuty() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 11, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 4, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(5, 0, PM, 10, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, PM, 12, 0, AM, SleeperBerth, theDevice);

        MainActions.navigateBack(theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 7, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(8, 0, AM, 9, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(9, 0, AM, 1, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(2, 0, PM, 6, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, PM, 8, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(8, 0, PM, 10, 0, PM, Driving, theDevice);

        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);
        checkThatViolationIconIsNOTDisplayedForTheRecord(7, theDevice);
        checkThatViolationIconIsNOTDisplayedForTheRecord(8, theDevice);
    }

    // Case #9
    @Test(priority = 9)
    public void testExampleThreeTheDrivingLimit() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 11, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 4, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(5, 0, PM, 11, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, PM, 12, 0, AM, OnDuty, theDevice);

        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);
        checkThatViolationIconIsNOTDisplayedForTheRecord(8, theDevice);
    }

    // Case #10
    @Test(priority = 10)
    public void testExampleSevenTheRestBreak() throws UiObjectNotFoundException, InterruptedException {


        HoursOfServiceActions.scrollHoSInterface(999, theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 11, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 6, 0, PM, Driving, theDevice);;
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, PM, 8, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(8, 30, PM, 12, 0, AM, Driving, theDevice);

        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);
        checkThatViolationIconIsNOTDisplayedForTheRecord(8, theDevice);
    }

    // Case #11
    @Test(priority = 11)
    public void testExampleTenTheThirtyFourHourRestart() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 1, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 3, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(5, 0, AM, 11, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, PM, 1, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(9, 0, PM, 12, 0, AM, SleeperBerth, theDevice);

        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 8, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, PM, 12, 0, AM, Driving, theDevice);

        MainActions.navigateBack(theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(6, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 4, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(5, 0, AM, 9, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 12, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, PM, 2, 0, PM, OnDuty, theDevice);

        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);

        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);

        checkThatViolationIconIsNOTDisplayedForTheRecord(6, theDevice);
        checkThatViolationIconIsNOTDisplayedForTheRecord(7, theDevice);
        checkThatViolationIconIsNOTDisplayedForTheRecord(8, theDevice);
    }

    // Case #12
    @Test(priority = 12)
    public void testExampleFourteenTheTwoHoursOffDutyCombinedWithSleeperBerth() throws UiObjectNotFoundException, InterruptedException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 2, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(2, 0, AM, 6, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, AM, 2, 0, PM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, PM, 5, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(5, 0, PM, 8, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(9, 0, PM, 12, 0, AM, Driving, theDevice);

        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);

        checkThatViolationIconIsNOTDisplayedForTheRecord(8, theDevice);
    }

    // Case #13
    @Test(priority = 13)
    public void testExampleSeventeenSleeperBerthUse() throws InterruptedException, UiObjectNotFoundException {
        HoursOfServiceActions.scrollHoSInterface(999, theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 6, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, PM, 12, 0, AM, SleeperBerth, theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 2, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(2, 0, AM, 4, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, AM, 6, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, AM, 1, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(1, 30, PM, 3, 30, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(3, 30, PM, 4, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, PM, 12, 0, AM, SleeperBerth, theDevice);

        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);

        checkThatViolationIconIsNOTDisplayedForTheRecord(7, theDevice);
        checkThatViolationIconIsNOTDisplayedForTheRecord(8, theDevice);
    }

    // Case #14
    @Test(priority = 14)
    public void testExampleNineteenSleeperBerthUse() throws InterruptedException, UiObjectNotFoundException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(2, 0, AM, 3, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(3, 0, AM, 8, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(8, 0, AM, 10, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 4, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, PM, 12, 0, AM, SleeperBerth, theDevice);

        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 1, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(1, 0, AM, 5, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(5, 0, AM, 7, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(7, 0, AM, 1, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(1, 0, PM, 9, 0, PM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(9, 0, PM, 12, 0, PM, Driving, theDevice);

        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);

        checkThatViolationIconIsNOTDisplayedForTheRecord(7, theDevice);
        checkThatViolationIconIsNOTDisplayedForTheRecord(8, theDevice);
    }


    @Test(priority = 15)
    public void testThirtyFourHoursRestartLongCase() throws UiObjectNotFoundException, InterruptedException {
        HoursOfServiceActions.scrollHoSInterface(999, theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(8, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 11, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 5, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, PM, 7, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(7, 0, PM, 9, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(9, 0, PM, 10, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, PM, 12, 0, AM, Driving, theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(7, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 10, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 11, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 4, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(5, 0, PM, 8, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(8, 0, PM, 10, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, PM, 11, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, PM, 12, 0, AM, OnDuty, theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(6, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 12, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, PM, 4, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(5, 0, PM, 7, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(9, 0, PM, 12, 0, AM, OnDuty, theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(5, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 8, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(8, 0, AM, 10, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 11, 0, AM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, AM, 1, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(3, 0, PM, 6, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(7, 0, PM, 10, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, PM, 12, 0, AM, OnDuty, theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(4, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 8, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(8, 0, AM, 10, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, PM, 3, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(3, 0, PM, 4, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(4, 0, PM, 7, 0, PM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(7, 0, PM, 10, 0, PM, OnDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, PM, 12, 0, AM, SleeperBerth, theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(3, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 6, 0, AM, SleeperBerth, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(6, 0, AM, 10, 0, AM, Driving, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, AM, 1, 0, PM, OnDuty, theDevice);
        MainActions.navigateBack(theDevice);

        HoursOfServiceActions.openDailyLogRecordByRowNumber(2, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(12, 0, AM, 12, 0, AM, OffDuty, theDevice);
        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(11, 0, PM, 12, 0, AM, Driving, theDevice);
        checkViolationsCountEquals(0, theDevice);
        MainActions.navigateBack(theDevice);

        checkThatViolationIconIsNOTDisplayedForTheRecord(2, theDevice);
    }


    // Case #16
    // Violation 8, 70 hour+ test case, long (short is case #7), not from example doc.
    // Should be executed right after long 34 hours restart test case (#15).
    @Test(priority = 16)
    public void testViolationOfSeventyHoursDrivingWindowLong() throws InterruptedException, UiObjectNotFoundException {

        HoursOfServiceActions.scrollHoSInterface(999, theDevice);
        HoursOfServiceActions.openDailyLogRecordByRowNumber(2, theDevice);

        HoursOfServiceActions.DailyLogActions.addDutyStatusRecord(10, 0, PM, 12, 0, AM, Driving, theDevice);

        checkViolationOfSeventyHoursDrivingWindowIsDisplayed(theDevice);
        checkViolationsCountEquals(1, theDevice);
        MainActions.navigateBack(theDevice);

        checkThatViolationIconIsDISPLAYEDForTheRecord(2, theDevice);
    }

    // 16 cases, ~31 minutes+ run

}
